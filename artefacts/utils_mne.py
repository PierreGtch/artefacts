import numpy as np
import mne

from .utils_numpy import mask_to_intervals


def raw_to_epochs(raw, time_trials=None, n_trials=None):
    '''
    splits a raw signal into epochs
    '''
    sfreq = raw.info['sfreq']
    if time_trials is None:
        assert n_trials is not None, 'time_trials and n_trials can not be both None'
        time_trials = len(raw)/sfreq/n_trials
    events = np.arange(0, len(raw)/sfreq, time_trials) + time_trials/2.
    annotations = mne.Annotations(events, np.zeros(len(events)), ['trial']*len(events))
    old_annotations = raw.annotations ## keep existing annotations
    raw.set_annotations(annotations)
    events, event_id = mne.events_from_annotations(raw)
    epochs = mne.Epochs(raw, events, event_id, -time_trials/2, time_trials/2)
    epochs.drop_bad()
    raw.set_annotations(old_annotations) ## return existing annotations
    return epochs


def mask_to_annotation(mask, sfreq, intervals=None):
    '''
    returns a mne.Annotations object
    indicating the Thue area of the mask

    mask computed by the threshold._find_artefacts method

    optionaly, intervals can be precomputed
    '''
    if intervals is None:
        intervals = mask_to_intervals(mask)
    lengths = intervals[:,1] - intervals[:,0] + 1
    return mne.Annotations(intervals[:,0]/sfreq, lengths/sfreq, ['bad artefact']*len(intervals))
