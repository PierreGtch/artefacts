'''

visualization utilitarian functions

'''
import numpy as np
import matplotlib.pyplot as plt



def plot_cleaning(X, X_clean=None, X_soft=None, title=None, ylim=(-0.0006, .0006), t=None):
    '''
    plots the output of bss.clean_signal method
    '''
    if t is None:
        t = np.arange(X.shape[-1])
    plt.figure()
    if title is not None:
        plt.suptitle(title)
    for i, d in enumerate(X):
        plt.subplot(len(X), 1, i+1)
        plt.title(f'channel {i}')
        plt.plot(t, d, label='original')
        if X_soft is not None:
            plt.plot(t, X_soft[i], label='dwt_denoised')
        if X_clean is not None:
            plt.plot(t, X_clean[i], label="cleaned")
        if ylim is not None:
            plt.ylim(*ylim)
        plt.legend()

def plot_sources(sources, title=None, ylim=None, t=None):
    plt.figure()
    if title is not None:
        plt.suptitle(title)
    for i, c in enumerate(sources):
        plt.subplot(len(sources), 1, i+1)
        plt.title(f'source {i}')
        if t is None:
            plt.plot(c)
        else:
            plt.plot(t, c)
        if ylim is not None:
            plt.ylim(*ylim)
        # plt.legend()
