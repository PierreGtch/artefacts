'''

Method implementing artefacts removal based on Blind Source Separation

the estimated artefactual sources are substracted to the signal

'''
import numpy as np
from sklearn.cross_decomposition import CCA
from sklearn.decomposition import FastICA

# from pywt import wavedec, waverec, threshold
from sobi import sobi ## https://github.com/davidrigie/sobi

from .utils_spectrum import dwt_denoising


def _get_sources(X_i, method='sobi', centering=0, **kwargs):
    '''
    Args:
        X_i: array, shape (n_channels, n_times,)
        sep_method: str in {'sobi' | 'CCA' | 'FastICA'}
            source separation method
        centering: int in {0|1|2}
            see _get_sources()
        n_components: optional int
            if not None: number of components to use during source separation
            else: set to n_channels
    Returns:
        S: array, shape (n_components, n_times,)
            sources
        A: array, shape (n_channels, n_components,)
            2D array containing mixing matrix
            i.e. A.dot(S) = X_i
        W: array, shape (n_components, n_channels,)
            2D array containing unmixing matrix
            i.e. W.dot(X) = S
    '''
    n_channels, n_times = X_i.shape
    if method=='sobi':
        S, A, W = sobi(X_i, **kwargs)
        return S, A, W
    kwargs.setdefault('n_components', n_channels)
    if method=='CCA':
        dec = lambda a,b: CCA(**kwargs).fit_transform(a,b)[0]
    elif method=='FastICA':
        dec = FastICA(**kwargs).fit_transform
    else:
        raise ValueError(f'unknown method {method}')

    if centering==0:
        params = (X_i[:,1:], X_i[:,:-1])
    elif centering==1:
        params = (X_i[:,:-1], X_i[:,1:])
    elif centering==2:
        params = (X_i, X_i)
    else:
        raise ValueError(f'invalid centering {centering}')
    W = dec(*params)
    ## remove null columns :
    null_col = np.all(W==0, axis=0)
    W = W[:,~null_col]
    S = W.T.dot(X_i)
    A = _get_u(X_i, S)
    return S / np.linalg.norm(S, axis=-1, keepdims=True), A, W

def _get_u(X_i, sources):
    """
    solves $u = argmax_u sum_k|u_k.v_k^T - X_i|_2$
    with v = sources

    Args:
        X_i: array, shape (n_channels, n_times,)
        sources: array, shape (n_components, n_times,)
    Returns:
        u: array, shape (n_components, n_channels,)
    """
    u = sources.dot(X_i.T) / (sources**2).sum(axis=-1, keepdims=True)
    return u

def _contains_artefact(X_i, threshold=1, method=None):
    '''
    heuristic that tells if the signal contains an artefact
    '''
    ## TODO :
    return True

def decide_th_max(threshold):
    '''
    returns a decision_func (for the clean_signal method)
    that declares as artefact the source that, once reconstructed,
    have at least one sample in one channel above threshold
    '''
    return lambda S,A: (A.max(axis=0)*S.max(axis=1)) > threshold

def clean_signal(X, S=None, A=None, bss_args=dict(), dwt_args=None, copy_X=True, decision_func=decide_th_max(1e-3), test_method=None, threshold=1,):
    '''
    removes artefacts from the signal X

    Args:
        X: array, shape (n_trials, n_channels, n_times,)
            multi channels signal
        bss_args: dict
            see _get_sources()
        dwt_args: dict
            see dwt_denoising()
        decision_func: function
            func that returns which sources are artefacts
        copy_X: boolean

    Returns:
        X_clean: array, shape (n_trials, n_channels, n_times,)
    '''
    ## TODO: If we want to rejoin the trials, there is no guarantee.
    # n_trials, n_channels, n_times, = X.shape
    X_clean = X.copy() if copy_X else X
    for i, X_i in enumerate(X_clean):
        if not _contains_artefact(X_i, threshold=threshold, method=test_method):
            continue
        if S is None or A is None:
            S_i, A_i, _ = _get_sources(X_i, **bss_args)
        else:
            S_i, A_i = S[i], A[i]
        idx_art = decision_func(S_i, A_i)
        X_i -= A_i[:,idx_art].dot(S_i[idx_art])
    return X_clean
