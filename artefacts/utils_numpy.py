import numpy as np

def mask_to_intervals(mask):
    '''
    returns the list of  intervals where the mask is continuously True
    '''
    transitions = np.convolve([-1,1], np.concatenate([[-1],mask*2-1,[-1]]), mode='valid')
    start = np.arange(mask.size)[transitions[:-1]==-2]
    stop  = np.arange(mask.size)[transitions[1 :]== 2]
    assert start.size==stop.size
    return np.stack([start, stop], axis=1)

def intervals_to_mask(intervals, n_times):
    '''
    intervals: list of pairs of index
    n_times: length (in samples) of the desired mask
    '''
    out = np.zeros(n_times, dtype=np.bool)
    for i0,i1 in intervals:
        out[i0:i1+1] = True
    return out

def mooving_avg(X, n, axis=-1):
    '''
     computes a moving average of X over n values along axis
    '''
    out = np.cumsum(X, axis=axis)
    idx = np.arange(X.shape[axis])
    ## would be faster without take but I found no way to enable custom axis without take
    return (np.take(out, idx[n:], axis) - np.take(out, idx[:-n], axis)) / n
