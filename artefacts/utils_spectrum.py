import numpy as np
from scipy.fft import rfft, irfft, rfftfreq
from scipy.integrate import simps
import pywt


# (delta, theta, alpha, beta, gamma) bands
EEG_BANDS = ((.5, 3.5), (3.5, 7.5), (7.5, 13.), (13., 35.), (35., 128.))
EEG_BANDS_NAMES = ['delta', 'theta', 'alpha', 'beta', 'gamma']
BAND_LABELS1 = [f'{a}-{b}' for (a,b) in EEG_BANDS]
BAND_LABELS2 = [f'{n}({a}-{b})' for (a,b),n in zip(EEG_BANDS, EEG_BANDS_NAMES)]

def integrated_psd(X, sfreq, freq_bands=EEG_BANDS, axis=-1):
    '''
    computes integrated spectrum power over frequency bands
    '''
    freq = rfftfreq(X.shape[axis], 1./sfreq)
    d = freq[1]-freq[0]
    spectrum = np.absolute(rfft(X, axis=axis))
    out = []
    for b_min,b_max in freq_bands:
        if not b_min<=b_max:
            raise ValueError("bad frequency band")
        indices = np.arange(freq.size)[(freq>=b_min) & (freq<b_max)]
        if indices.size==0:
            print
            raise ValueError(f'empty band {(b_min,b_max)}\nwith freqs {freq}')
        # out.append( np.take(spectrum, indices, axis=axis).sum(axis=axis) * d )
        # integrate :
        out.append( simps(np.take(spectrum, indices, axis=axis), dx=d, axis=axis) )
    return np.stack(out, axis=axis)


def dwt_denoising(X, threshold=1e-5, wavelet='db1', mode='per', th_mode='hard'):
    # Decompose to get the wavelet coefficients
    coeff = pywt.wavedec( X, wavelet, mode=mode)
    # Calculte the univeral threshold
    coeff[1:] = [pywt.threshold(i, value=threshold, mode=th_mode) for i in coeff[1:]]
    # Reconstruct the signal using the thresholded coefficients
    return pywt.waverec( coeff, wavelet, mode=mode)
