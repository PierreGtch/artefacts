'''

Artefact removal based on M-estimators and thresholding

artefactual samples (and vicinity) are cutted off from the signal

'''
import numpy as np
from math import ceil
from .utils_numpy import intervals_to_mask, mask_to_intervals, mooving_avg


def _find_artefacts_online(Xi, n_window, max_ratio, n_radius, n_min, n_preheat, n_stride, eps=1e-6):
    """
    Args:
        Xi: array, shape (n_times,)
            input signal with 0 mean
        n_window: int
            size (in samples) of the window used to compute local std
        max_ratio: float
            maximum ratio between local and global std
        n_radius: int
            size of the signal (in samples) to remove around each artefact detection
        n_min: int
            minimal length (in samples) required for chunk of signal to be kept
        n_preheat: int
            duration (in samples) of the preheating phase
        n_stride: int
            stride (in samples)
        eps: float
            0-mean test parameter
    Returns:
        good_parts: array, shape (n_intervals, 2)
            index pairs of the intervals selected in Xs
    """
    assert Xi.mean() < eps, 'X must have a mean close to 0'
    # TODO complexity can be divided by 3 by using a queue to keep track sums in window
    max_ratio = max_ratio**2
    good_parts = []
    start = 0
    is_art = False
    mid = n_window//2
    assert n_preheat>=mid, 'n_preheat < n_window//2'
    local_agg = np.sum(Xi[n_preheat-mid:n_preheat-mid+n_window]**2)
    global_agg = np.sum(Xi[:n_preheat]**2)
    count = n_preheat
    for j in range(n_preheat-mid, len(Xi)-n_window, n_stride):
        local_agg += np.sum(Xi[j+n_window:j+n_window+n_stride]**2) - np.sum(Xi[j:j+n_stride]**2)
        ratio = (local_agg/n_window) / (global_agg/count)
        if ratio > max_ratio:
            if not is_art:
                end = j-1+mid-n_radius
                if end-start+1 >= n_min:
                    good_parts.append((start,end))
                start = None
                is_art = True
        else:
            global_agg += np.sum(Xi[j+mid:j+mid+n_stride]**2)
            count += n_stride
            if is_art:
                start = j+mid+n_radius
                is_art = False
    end = len(Xi)-1
    if (start is not None) and (end-start+1 >= n_min):
        good_parts.append((start,end))
    return np.array(good_parts)

def _find_artefacts(X, n_window, max_ratio=2., eps=1e-6):
    '''
    find artefacts using their norm

    Args:
        X: array, shape (n_trials, n_channels, n_times,)
            multi channels signal
        n_window: int
            size (in samples) of the window used to compute local std
        max_ratio: float
            maximum ratio between local and global std
        eps: float
            0-mean test parameter
    Returns:
        mask: array, shape (n_trials, n_channels, n_times,)
            boolean mask specifying where are located the artefacts
    '''
    assert X.mean() < eps, 'X must have a mean close to 0'
    n_trials, n_channels, n_times, = X.shape
    mask = np.zeros(X.shape, dtype=np.bool)
    window = np.ones(n_window) / n_window
    for Xi, mi in zip(X, mask):
        for Xij, mij in zip(Xi, mi):
            Xsq = Xij**2
            local_std = np.convolve(Xsq, window, mode='same')**.5
            while True: ## loop until convergence
                new_m = (local_std/Xsq[~mij].mean()**.5) > max_ratio
                if np.all(new_m==mij):
                    break
                mij[:] = new_m
            # std2 = np.median(np.abs(local_std**2-Xsq.mean()))*1.4826*(1+5/(len(local_std)-1))
            # std3 = np.median(np.abs(local_std**2-Xsq[~mij].mean()))*1.4826*(1+5/(len(local_std)-1))
            # std4 = (np.median(np.abs(local_std**2-Xsq.mean()))*1.4826*(1+5/(len(local_std)-1)))**.5
            # std5 = (np.median(np.abs(local_std**2-Xsq[~mij].mean()))*1.4826*(1+5/(len(local_std)-1)))**.5
            # print(f'original std :{Xsq.mean()**.5}, estimated std : {Xsq[~mij].mean()**.5}, other estimated std : {std2, std3, std4, std5}')
    return mask

def _spread_mask(mask, n, copy=True):
    '''
    set to True the elements in a radius of n samples
    around each True value in the mask
    '''
    if copy:
        out = mask.copy()
    else:
        out = mask
        mask = mask.copy()
    for i in range(-n, 0):
        np.logical_or(out[:i], mask[-i:], out=out[:i])
    for i in range(1, n+1):
        np.logical_or(out[i:], mask[:-i], out=out[i:])
    return out

def _fill_gaps(mask, n_min, intervals_false=None, copy=True):
    '''
    set the continuous False intervals to True if their lengths are
    smaller than n_min samples

    optionaly, intervals_false can be precomputed
    '''
    if copy:
        mask = mask.copy()
    if intervals_false is None:
        intervals_false = mask_to_intervals(~mask)
    lengths = intervals_false[:,1] - intervals_false[:,0] + 1
    for (i0,i1), l in zip(intervals_false, lengths):
        if l<n_min:
            mask[i0:i1+1] = True
    return mask

def find_artefacts(X, sfreq, t_window=5., max_ratio=2., artefact_radius=5., min_length=60., online=False, preheat=20., stride=.5):
    """
    finds and remove the zones in a raw (but 0-mean) signal containing artefacts

    Args:
        X: array, shape (n_channels, n_times)
            input signal with 0 mean
        sfreq: float
            sampling frequency
        t_window: float
            size (in seconds) of the window used to compute local std
        max_ratio: float
            maximum ratio between local and global std
        artefact_radius: float
            size of the signal (in seconds) to remove around each artefact detection
        min_length: float
            minimal length (in seconds) required for chunk of signal to be kept
        online: bool
            use the online algorithm _find_artefacts_online or _find_artefacts
        preheat: float
            duration (in seconds) of the preheating phase of the online algo
        stride: float
            stride (in seconds) in the online algo
    Returns:
        good_parts: array, shape (n_intervals, 2)
            index pairs of the intervals selected in Xs
        mask: array, shape (n_times,)
            mask specifying qhere are located the artefacts
    """
    n_channels, n_times = X.shape
    if online:
        mask = np.zeros(n_times, dtype=np.bool)
        ## TODO : parallelize that
        for Xi in X: ## iterate over channels
            good_parts = _find_artefacts_online(Xi, int(t_window*sfreq), max_ratio, int(artefact_radius*sfreq), int(min_length*sfreq), int(preheat*sfreq), int(stride*sfreq))
            mask += ~intervals_to_mask(good_parts, n_times)
        if X.shape[0]>1:
            good_parts = mask_to_intervals(~mask)
    else:
        mask = _find_artefacts(X[None,:], n_window=int(t_window*sfreq), max_ratio=max_ratio)[0].max(axis=0)
        _ = _spread_mask(mask, int(artefact_radius*sfreq), copy=False)
        _ = _fill_gaps(mask, int(min_length*sfreq), copy=False)
        good_parts = mask_to_intervals(~mask)
    return good_parts, mask

def cut_artefacts(X, sfreq, t_window=5., max_ratio=2., artefact_radius=5., min_length=60., online=False, preheat=20., stride=.5):
    """
    finds and remove the zones in a raw (but 0-mean) signal containing artefacts

    Args:
        X: array, shape (n_channels, n_times)
            input signal with 0 mean
        sfreq: float
            sampling frequency
        t_window: float
            size (in seconds) of the window used to compute local std
        max_ratio: float
            maximum ratio between local and global std
        artefact_radius: float
            size of the signal (in seconds) to remove around each artefact detection
        min_length: float
            minimal length (in seconds) required for chunk of signal to be kept
        online: bool
            use the online algorithm _find_artefacts_online or _find_artefacts
        preheat: float
            duration (in seconds) of the preheating phase of the online algo
        stride: float
            stride (in seconds) in the online algo
    Returns:
        Xs: list (length n_intervals) of arrays, shape (n_channels, chunk_length)
            list of atefact-free signal chunks
        good_parts: array, shape (n_intervals, 2)
            index pairs of the intervals selected in Xs
        mask: array, shape (n_times,)
            mask specifying qhere are located the artefacts
    """
    good_parts, mask = find_artefacts(
        X, sfreq=sfreq, t_window=t_window, max_ratio=max_ratio, preheat=preheat,
        artefact_radius=artefact_radius, min_length=min_length, online=online, stride=stride,
    )
    Xs = [X[:,i0:i1+1] for i0,i1 in good_parts]
    return Xs, good_parts, mask

def list_to_padded_array(Xs, intervals, n_times):
    '''
    transforms a list of various lengths signals to a padded array

    Args:
        Xs: list (length n_intervals) of arrays, shape (n_channels, chunk_length)
            list of atefact-free signal chunks
        intervals: array, shape (n_intervals, 2)
            pairs of index (in raw data) of the intervals selected in Xs
        n_times: int
            length (in samples) of the desired array
    Returns:
        X: array, shape (n_trials, n_channels, n_times)
            padded array
        new_intervals: array, shape (n_trials, 2)
            index pairs of the intervals selected in X
    '''
    n_channels,_ = Xs[0].shape
    Xss = []
    new_intervals = []
    for a,(idx0,idx1) in zip(Xs, intervals):
        n = a.shape[1]
        if n <= n_times:
            Xss.append(a)
            new_intervals.append([idx0,idx1])
            continue
        # making split of the same size:
        n_split = int(ceil(n/n_times))
        width = int(ceil(n/n_split))
        for i in range(n_split):
            Xss.append(a[:,i*width:min((i+1)*width, n)])
            new_intervals.append([idx0,min(idx0+width-1, idx1)])
            idx0 += width
    new_intervals = np.array(new_intervals, dtype=np.int32)
    X = np.zeros((len(Xss), n_channels, n_times), dtype=Xss[0].dtype)
    for a,o in zip(Xss, X):
        o[:,:a.shape[1]] = a
    return X, new_intervals
