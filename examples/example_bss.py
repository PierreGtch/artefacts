import os
import numpy as np
import matplotlib.pyplot as plt
import mne
from artefacts import clean_signal, decide_th_max, plot_cleaning, plot_sources
from artefacts import bss, utils_mne

# set directory and path
datadir = os.path.expanduser('~/data/EEGData')
rawpath = os.path.join(datadir, '20190503_14h41_3-1a-256Hz-raw.fif')

# get data :
num_epochs = 10
raw = mne.io.read_raw_fif(rawpath, preload=False)
sfreq = raw.info['sfreq']
epochs = utils_mne.raw_to_epochs(raw, time_trials=20) # time_trials in seconds
X = epochs[:num_epochs].get_data()
X -= X.mean(axis=-1, keepdims=True)


# cleaning :
output = [bss._get_sources(X_i, method='sobi') for X_i in X]
S, A, W = [np.array(a) for a in zip(*output)]

X_clean = clean_signal(
    X,           # signal
    S=S,         # precomputed sources
    A=A,         # precomputed mixing matrix
    bss_args=None,
    dwt_args=None,
    copy_X=True,
    decision_func=decide_th_max(1e-3)
)

# plotting :
epoch_idx = 0

t = np.arange(X.shape[-1]) / sfreq # time array
plot_cleaning(X[epoch_idx], X_clean=X_clean[epoch_idx], title=f'before and after cleaning (epoch {epoch_idx})', t=t)
plot_sources(S[epoch_idx], title=f'extracted sources (epoch {epoch_idx})', t=t)
plt.show()
