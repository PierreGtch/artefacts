"""
remove artefacts then learn dict on clean data
"""
import numpy as np
from time import time
import os
import matplotlib.pyplot as plt
import mne

from artefacts import cut_artefacts, list_to_padded_array
from artefacts.utils_mne import mask_to_annotation

# set directory and path
datadir = os.path.expanduser('~/data/EEGData')
rawpath = os.path.join(datadir, '20190503_14h41_3-Ch1-1a-raw.fif')

# get data :
num_epochs = 10
raw = mne.io.read_raw_fif(rawpath, preload=True)
raw.filter(l_freq=.1, h_freq=None)
sfreq = raw.info['sfreq']

# remove artefacts :
kwargs_art = dict(
    t_window=5,
    max_ratio=2.,
    artefact_radius=5.,
    min_length=60.,
    online=True,
    preheat=20.,
    stride=.5,
)
t0 = time()
print('removing artefacts...')
Xs, intervals, mask = cut_artefacts(raw.get_data(), sfreq, **kwargs_art)
t1 = time()
print(f'...done in {t1-t0}s.')
print('intervals :')
print(np.array_repr(intervals).replace('\n', '').replace(' ', ''))

# generating padded epoched data, without artefacts :
X, intervals = list_to_padded_array(Xs, intervals, n_times=int(kwargs_art['min_length']*sfreq))
epochs = mne.EpochsArray(X, info=raw.info)

## plot artefacts :
raw.set_annotations(mask_to_annotation(mask, sfreq))
raw.plot(scalings='auto', duration=100,    block=False)
epochs.plot(scalings='auto', n_epochs=3, block=True)
