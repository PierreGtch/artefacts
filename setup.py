import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="artefacts",
    version="0.0.1",
    author="Pierre Guetschel",
    author_email="pierre.guetschel@gmail.com",
    description="EEG artefacts handeler",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/no_repo_yet...",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
