Module artefacts.utils_spectrum
===============================

Functions
---------

    
`dwt_denoising(X, threshold=1e-05, wavelet='db1', mode='per', th_mode='hard')`
:   

    
`integrated_psd(X, sfreq, freq_bands=((0.5, 3.5), (3.5, 7.5), (7.5, 13.0), (13.0, 35.0), (35.0, 128.0)), axis=-1)`
:   computes integrated spectrum power over frequency bands