Module artefacts.bss
====================
Method implementing artefacts removal based on Blind Source Separation

the estimated artefactual sources are substracted to the signal

Functions
---------

    
`clean_signal(X, S=None, A=None, bss_args={}, dwt_args=None, copy_X=True, decision_func=<function decide_th_max.<locals>.<lambda>>, test_method=None, threshold=1)`
:   removes artefacts from the signal X
    
    Args:
        X: array, shape (n_trials, n_channels, n_times,)
            multi channels signal
        bss_args: dict
            see _get_sources()
        dwt_args: dict
            see dwt_denoising()
        decision_func: function
            func that returns which sources are artefacts
        copy_X: boolean
    
    Returns:
        X_clean: array, shape (n_trials, n_channels, n_times,)

    
`decide_th_max(threshold)`
:   returns a decision_func (for the clean_signal method)
    that declares as artefact the source that, once reconstructed,
    have at least one sample in one channel above threshold