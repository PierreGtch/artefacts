Module artefacts.utils_numpy
============================

Functions
---------

    
`intervals_to_mask(intervals, n_times)`
:   intervals: list of pairs of index
    n_times: length (in samples) of the desired mask

    
`mask_to_intervals(mask)`
:   returns the list of  intervals where the mask is continuously True

    
`mooving_avg(X, n, axis=-1)`
:   computes a moving average of X over n values along axis