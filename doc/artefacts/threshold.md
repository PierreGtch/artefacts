Module artefacts.threshold
==========================
Artefact removal based on M-estimators and thresholding

artefactual samples (and vicinity) are cutted off from the signal

Functions
---------

    
`cut_artefacts(X, sfreq, t_window=5.0, max_ratio=2.0, artefact_radius=5.0, min_length=60.0, online=False)`
:   finds and remove the zones in a raw (but 0-mean) signal containing artefacts
    
    Args:
        X: array, shape (n_channels, n_times)
            input signal with 0 mean
        sfreq: float
            sampling frequency
        t_window: float
            size (in seconds) of the window used to compute local std
        max_ratio: float
            maximum ratio between local and global std
        artefact_radius: float
            size of the signal (in seconds) to remove around each artefact detection
        min_length: float
            minimal length (in seconds) required for chunk of signal to be kept
        online: bool
            use the online algorithm _find_artefacts_online or _find_artefacts
    Returns:
        Xs: list (length n_intervals) of arrays, shape (n_channels, chunk_length)
            list of atefact-free signal chunks
        good_parts: array, shape (n_intervals, 2)
            index pairs of the intervals selected in Xs
        mask: array, shape (n_times,)
            mask specifying qhere are located the artefacts

    
`list_to_padded_array(Xs, intervals, n_times)`
:   transforms a list of various lengths signals to a padded array
    
    Args:
        Xs: list (length n_intervals) of arrays, shape (n_channels, chunk_length)
            list of atefact-free signal chunks
        intervals: array, shape (n_intervals, 2)
            pairs of index (in raw data) of the intervals selected in Xs
        n_times: int
            length (in samples) of the desired array
    Returns:
        X: array, shape (n_trials, n_channels, n_times)
            padded array
        new_intervals: array, shape (n_trials, 2)
            index pairs of the intervals selected in X