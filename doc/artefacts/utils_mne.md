Module artefacts.utils_mne
==========================

Functions
---------

    
`mask_to_annotation(mask, sfreq, intervals=None)`
:   returns a mne.Annotations object
    indicating the Thue area of the mask
    
    mask computed by the threshold._find_artefacts method
    
    optionaly, intervals can be precomputed

    
`raw_to_epochs(raw, time_trials=None, n_trials=None)`
:   splits a raw signal into epochs