Module artefacts.viz
====================
visualization utilitarian functions

Functions
---------

    
`plot_cleaning(X, X_clean=None, X_soft=None, title=None, ylim=(-0.0006, 0.0006), t=None)`
:   plots the output of bss.clean_signal method

    
`plot_sources(sources, title=None, ylim=None, t=None)`
: